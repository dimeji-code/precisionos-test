import {
  Typography,
  Box,
  Divider,
  ListItemButton,
  List,
  ListSubheader,
  ListItemIcon,
} from '@mui/material';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import GroupIcon from '@mui/icons-material/Group';
import TableChartIcon from '@mui/icons-material/TableChart';
import SupportIcon from '@mui/icons-material/Support';
import DateRangeIcon from '@mui/icons-material';
import ArticleIcon from '@mui/icons-material/Article';
import BarChartIcon from '@mui/icons-material/BarChart';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import Button from '~/Button/src/Button';

const madShadow =
  '4px 4.0px 9.2px rgba(0, 0, 0, 0.06),3px 3.7px 13.3px rgba(60, 66, 80, 0.2), 2px 4.5px 6px rgba(42, 56, 102, 0.1),0 4.5px 5.9px rgba(0, 0, 0, 0.1)';

const SideNavigation = (props: any) => {
  const [currentTab, setCurrentTab]: any = useState(props.current);

  const handleListItemClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    index: number
  ) => {
    setCurrentTab(index);
  };

  return (
    <Box sx={{ justifyContent: 'space-between', boxShadow: madShadow }}>
      <div>
        <List
          sx={{
            width: '100%',
            bgcolor: 'background.paper',
            marginTop: '0.5px',
          }}
          component="nav"
          aria-labelledby="nested-list-subheader"
          subheader={
            <ListSubheader component="div" id="nested-list-subheader">
              ADMINISTRATOR
            </ListSubheader>
          }
        >
          <Link
            key={0}
            style={{ textDecoration: 'none', color: 'black' }}
            to={'/'}
          >
            <ListItemButton
              selected={currentTab === 0}
              onClick={(event) => handleListItemClick(event, 0)}
            >
              <ListItemIcon>
                <BarChartIcon />
              </ListItemIcon>
              <Typography className="sideNavText">Admin Dashboard</Typography>
              {/* <ListItemText className="sideNavText" style={{fontSize:10}} primary="Admin Dashboard" /> */}
            </ListItemButton>
          </Link>

          <Link
            key={1}
            style={{ textDecoration: 'none', color: 'black' }}
            to={'/allProcedures'}
          >
            <ListItemButton
              selected={currentTab === 1}
              onClick={(event) => handleListItemClick(event, 1)}
            >
              <ListItemIcon>
                <ArticleIcon />
              </ListItemIcon>
              <Typography className="sideNavText">All Procedures</Typography>
              {/* <ListItemText primary="All Procedures" /> */}
            </ListItemButton>
          </Link>

          <Link
            key={2}
            style={{ textDecoration: 'none', color: 'black' }}
            to={'/allSessions'}
          >
            <ListItemButton
              selected={currentTab === 2}
              onClick={(event) => handleListItemClick(event, 2)}
            >
              <ListItemIcon>
                <TableChartIcon />
              </ListItemIcon>
              <Typography className="sideNavText">All Sessions</Typography>
              {/* <ListItemText primary="All Sessions" /> */}
            </ListItemButton>
          </Link>

          <Divider variant="middle" />
          <ListSubheader component="div" id="nested-list-subheader">
            PERSONAL
          </ListSubheader>

          <Link
            key={3}
            style={{ textDecoration: 'none', color: 'black' }}
            to={'/myDashboard'}
          >
            <ListItemButton
              selected={currentTab === 3}
              onClick={(event) => handleListItemClick(event, 3)}
            >
              <ListItemIcon>
                <BarChartIcon />
              </ListItemIcon>
              <Typography className="sideNavText">My Dashboard</Typography>
              {/* <ListItemText primary="My Dashboard" /> */}
            </ListItemButton>
          </Link>

          <Link
            key={4}
            style={{ textDecoration: 'none', color: 'black' }}
            to={'/myProcedures'}
          >
            <ListItemButton
              selected={currentTab === 4}
              onClick={(event) => handleListItemClick(event, 4)}
            >
              <ListItemIcon>
                <ArticleIcon />
              </ListItemIcon>
              <Typography className="sideNavText">My Procedures</Typography>
              {/* <ListItemText primary="My Procedures" /> */}
            </ListItemButton>
          </Link>

          <Divider variant="middle" />

          <Link
            key={5}
            style={{ textDecoration: 'none', color: 'black' }}
            to={'/support'}
          >
            <ListItemButton
              selected={currentTab === 5}
              onClick={(event) => handleListItemClick(event, 5)}
            >
              <ListItemIcon>
                <SupportIcon />
              </ListItemIcon>
              <Typography className="sideNavText">Support </Typography>
              {/* <ListItemText primary="Support" /> */}
            </ListItemButton>
          </Link>

          <Link
            key={6}
            style={{ textDecoration: 'none', color: 'black' }}
            to={'/loginTracker'}
          >
            <ListItemButton
              selected={currentTab === 6}
              onClick={(event) => handleListItemClick(event, 6)}
            >
              <ListItemIcon>
                <LockOpenIcon />
              </ListItemIcon>
              <Typography className="sideNavText">Login Tracker </Typography>
              {/* <ListItemText primary="Login Tracker" /> */}
            </ListItemButton>
          </Link>

          <Link
            key={7}
            style={{ textDecoration: 'none', color: 'black' }}
            to={'/allUsers'}
          >
            <ListItemButton
              selected={currentTab === 7}
              onClick={(event) => handleListItemClick(event, 7)}
            >
              <ListItemIcon>
                <GroupIcon />
              </ListItemIcon>
              <Typography className="sideNavText">All Users</Typography>
              {/* <ListItemText primary="All Users" /> */}
            </ListItemButton>
          </Link>

          <Link
            key={8}
            style={{ textDecoration: 'none', color: 'black' }}
            to={'/geolocation'}
          >
            <ListItemButton
              selected={currentTab === 8}
              onClick={(event) => handleListItemClick(event, 8)}
            >
              <ListItemIcon>
                <LocationOnIcon />
              </ListItemIcon>
              <Typography className="sideNavText">
                Geolocation Analysis
              </Typography>
              {/* <ListItemText primary="Geolocation Analysis" /> */}
            </ListItemButton>
          </Link>

          <Divider variant="middle" />
          <ListItemButton
            style={{
              padding: 10,
              justifyContent: 'center',
              alignSelf: 'center',
              alignItems: 'center',
            }}
            selected={currentTab === 8}
            onClick={(event) => handleListItemClick(event, 8)}
          >
            <Link
              key={9}
              style={{ textDecoration: 'none', color: 'black' }}
              to={'/geolocation'}
            >
              <Button color="primary" title="Pairing Code" />
            </Link>
          </ListItemButton>
        </List>
      </div>
    </Box>
  );
};

export default SideNavigation;
