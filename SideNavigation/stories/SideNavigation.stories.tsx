import React from 'react';
import SideNavigation from '../src/SideNavigation';
import { BrowserRouter } from 'react-router-dom';

export default {
  title: 'Side Navigation',
  component: SideNavigation,
};

export const SideNav = () => (
  <BrowserRouter>
    <SideNavigation current={0} />
  </BrowserRouter>
);
