import React, { useState, ReactNode } from 'react';
import { Typography, Card, CardContent, Grid, Box } from '@mui/material';

const ThinCard = (props: any) => {
  const type: number = props.type;

  return (
    <Grid
      item
      xs={props.size * 2}
      sm={props.size * 2}
      md={props.size}
      lg={props.size}
    >
      {
        type == 1 && (
          // <Grid item xs={4} md ={4}>
          <Card style={{ flexGrow: 1, flexDirection: 'row', margin: 1 }}>
            <Grid container sx={{ display: 'flex' }}>
              <CardContent>
                <Typography
                  sx={{ fontSize: 14 }}
                  color="text.secondary"
                  gutterBottom
                >
                  {props.title}
                </Typography>
                <Typography variant="h5" component="div"></Typography>
                <Typography sx={{ mb: 1.5, fontSize: 22 }} color="text.primary">
                  {props.num}
                </Typography>
              </CardContent>
              <Box
                style={{
                  maxHeight: 110,
                  display: 'flex',
                  flexGrow: 1,
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                  padding: 10,
                }}
              >
                <img
                  style={{ maxHeight: 100, minHeight: 65 }}
                  src={props.img}
                />
              </Box>
            </Grid>
          </Card>
        )
        //  </Grid>
      }
      {
        type == 2 && (
          // <Grid item xs={11} sm={12}  md ={6} lg={6}>
          <Card style={{ flexGrow: 1, flexDirection: 'row', margin: 10 }}>
            <Grid container sx={{ display: 'flex' }}>
              <CardContent>
                <Typography
                  sx={{ fontSize: 12 }}
                  color="text.secondary"
                  gutterBottom
                >
                  {props.category}
                </Typography>
                <Typography
                  sx={{ fontSize: 19, fontWeight: 'bold' }}
                  color="text.primary"
                  gutterBottom
                >
                  {props.title}
                </Typography>
                <Typography variant="h5" component="div"></Typography>
                <Typography
                  sx={{ mb: 1.5, fontSize: 13 }}
                  color="text.secondary"
                >
                  {props.desc}
                </Typography>
              </CardContent>
              <Box
                style={{
                  maxHeight: 110,
                  display: 'flex',
                  flexGrow: 1,
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                  padding: 15,
                }}
              >
                <img
                  style={{ maxHeight: 100, minHeight: 65 }}
                  src={props.img}
                />
              </Box>
            </Grid>
          </Card>
        )
        // </Grid>
      }
    </Grid>
  );
};
export default ThinCard;
