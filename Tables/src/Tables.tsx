import React from 'react';
import { Box, TextField, Grid, Divider, Button } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TablePagination from '@mui/material/TablePagination';
import Pagination from '@mui/material/Pagination';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { styled } from '@mui/material/styles';
import CustomButton from '~/Button/src/Button';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.black,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const Tables = (props: any) => {
  const col = props.col;
  const rows: any = props.rowsData;
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const [email, setEmail]: any = React.useState('');
  const [procedure, setProcedure]: any = React.useState('');
  const [location, setLocation]: any = React.useState('');
  const [organization, setOrganization] = React.useState('');
  const [rowsState, setRowsState]: any = React.useState(rows);

  let numSelected: number = 0;
  let rowCount: number = 0;
  const onSelectAllClick = () => {};

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };
  const clearFields = () => {
    setEmail('');
    setProcedure('');
    setLocation('');
    setOrganization('');
  };
  const searchFunc = () => {
    let found = rows;
    if (location != '') {
      console.log('found rows', rows);

      found = found.filter(
        (row: any) => row.location.toLowerCase() === location.toLowerCase()
      );
      console.log('found loc', found);
    }
    if (procedure != '') {
      found = found.filter((row: any) => row.procedure == procedure);
      console.log('found proc', found);
    }

    return found;
  };
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const beginSearch: any = () => {
    setRowsState(searchFunc);
    console.log('rowsState =>', rowsState);
  };

  return (
    <div>
      {props.type == 'allSessions' && (
        <Grid container sx={{ padding: 2 }}>
          <Box sx={{ marginRight: 2 }}>
            {/* <Typography style={{fontSize:"13px", color:"grey"}}>User Email</Typography> */}
            <TextField
              id="outlined-basic"
              size="small"
              label="User Email"
              variant="outlined"
              value={email}
              onChange={(event) => {
                setEmail(event.target.value);
              }}
            />
          </Box>
          <Box sx={{ marginRight: 2 }}>
            {/* <Typography style={{fontSize:"13px", color:"grey"}}>Procedure</Typography> */}
            <TextField
              id="outlined-basic"
              size="small"
              label="Procedure"
              variant="outlined"
              value={procedure}
              onChange={(event) => {
                setProcedure(event.target.value);
              }}
            />
          </Box>
          <Box sx={{ marginRight: 2 }}>
            {/* <Typography style={{fontSize:"13px", color:"grey"}}>Location </Typography> */}
            <TextField
              size="small"
              id="outlined-basic"
              label="Location"
              variant="outlined"
              value={location}
              onChange={(event) => {
                setLocation(event.target.value);
              }}
            />
          </Box>
          <Box sx={{ marginRight: 1 }}>
            {/* <Typography style={{fontSize:"13px", color:"grey"}}>Organization / Group Name </Typography> */}
            <TextField
              size="small"
              id="outlined-basic"
              label="Organization"
              variant="outlined"
              value={organization}
              onChange={(event) => {
                setOrganization(event.target.value);
              }}
            />
          </Box>
          <Box sx={{ marginRight: 1, display: 'flex', alignSelf: 'end' }}>
            <CustomButton
              onPress={beginSearch}
              color="primary"
              title="Search"
            />
          </Box>
          <Box sx={{ marginRight: 1, display: 'flex', alignSelf: 'end' }}>
            <CustomButton
              onPress={clearFields}
              color="primary"
              title="Clear Search"
            />
          </Box>
        </Grid>
      )}
      <TablePagination
        rowsPerPageOptions={[5, 10, 20]}
        component="div"
        count={rowsState.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      <Divider />
      <Table
        sx={
          props.type != 'activeCountries'
            ? { minWidth: 700 }
            : { minWidth: 100 }
        }
        aria-label="customized table"
      >
        {/* TABLE COLUMN TITLES FOR EVERY TABLE EXCEPT MY DASHBOARD AND ACTIVE COUNTRIES */}
        {props.type != 'activeCountries' && props.type != 'myDashboard' && (
          <TableHead style={{ width: '100%' }}>
            <TableRow>
              {col.map((column: any) => (
                <StyledTableCell align="left">{column.label}</StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
        )}
        {/* TABLE COLUMN TITLES FOR MY DASHBOARD */}
        {props.type == 'myDashboard' && (
          <TableHead style={{ width: '100%' }}>
            <TableRow>
              <TableCell padding="checkbox">
                <Checkbox
                  color="primary"
                  indeterminate={numSelected > 0 && numSelected < rowCount}
                  checked={rowCount > 0 && numSelected === rowCount}
                  onChange={onSelectAllClick}
                  inputProps={{
                    'aria-label': 'select all desserts',
                  }}
                />
              </TableCell>
              {col.map((column: any) => (
                <StyledTableCell align="left">{column.label}</StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
        )}
        <TableBody>
          {rowsState
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row: any) => {
              return (
                <StyledTableRow>
                  {props.type == 'myDashboard' && (
                    <TableCell padding="checkbox">
                      <Checkbox
                        color="primary"
                        indeterminate={
                          numSelected > 0 && numSelected < rowCount
                        }
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{
                          'aria-label': 'select all desserts',
                        }}
                      />
                    </TableCell>
                  )}
                  {col.map((column: any) => {
                    return (
                      <StyledTableCell align="left">
                        {row[column.id]}
                      </StyledTableCell>
                    );
                  })}
                </StyledTableRow>
              );
            })}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[5, 10, 20]}
        component="div"
        count={rowsState.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </div>
  );
};

export default Tables;
