import React from 'react';
import { Meta, Story } from '@storybook/react';
import Tables from '../src/Tables';

const meta: Meta = {
  title: 'Tables',
  component: Tables,
};
interface Column {
  id:
    | 'startTime'
    | 'location'
    | 'user'
    | 'procedure'
    | 'duration'
    | 'precScore'
    | 'analytics';
  label: string;
  minWidth?: number;
  align?: 'right';
  format?: (value: number) => string;
}

const columns: Column[] = [
  { id: 'startTime', label: 'START TIME', minWidth: 170 },
  { id: 'location', label: 'LOCATION', minWidth: 100 },
  {
    id: 'user',
    label: 'USER',
    minWidth: 170,
    //   align: 'left',
    format: (value: number) => value.toLocaleString('en-US'),
  },
  {
    id: 'procedure',
    label: 'PROCEDURE',
    minWidth: 170,
    //   align: 'right',
    format: (value: number) => value.toLocaleString('en-US'),
  },
  {
    id: 'duration',
    label: 'DURATION',
    minWidth: 170,
    // align: 'right',
    format: (value: number) => value.toFixed(2),
  },
  {
    id: 'precScore',
    label: 'PRECISION SCORE',
    minWidth: 170,
    // align: 'right',
    format: (value: number) => value.toFixed(2),
  },
  {
    id: 'analytics',
    label: 'CLINICAL ANALYTICS',
    minWidth: 170,
    // align: 'right',
    format: (value: number) => value.toFixed(2),
  },
];
var rows = [
  {
    startTime: 'Feb 3, 2022 -4:36am',
    location: 'Monroe',
    user: 'John Kennedy',
    procedure: '1',
    duration: 'GCSOM',
    precScore: 'Unavailable',
    analytics: 'individual',
  },
  {
    startTime: 'Feb 4, 2022 -12:36pm',
    location: 'Scranton',
    user: 'Samuel Jackson',
    procedure: '2',
    duration: 'GCSOM',
    precScore: 'Unavailable',
    analytics: 'individual',
  },
  {
    startTime: 'Feb 6, 2022 -3:36am',
    location: 'UpsideDown',
    user: 'Aubama who',
    procedure: '3',
    duration: 'GCSOM',
    precScore: 'Unavailable',
    analytics: 'individual',
  },
  {
    startTime: 'Feb 8, 2022 -4:16am',
    location: 'UpsideDown',
    user: 'Aubama what',
    procedure: '4',
    duration: 'GCSOM',
    precScore: 'Unavailable',
    analytics: 'individual',
  },
];
const columns2: readonly any[] = [
  { id: 'user', label: 'USER', width: 165 },
  { id: 'startTime', label: 'START TIME', width: 150 },
  { id: 'procedure', label: 'PROCEDURE', width: 350 },
  {
    id: 'duration',
    label: 'DURATION',
    width: 100,
  },
  {
    id: 'clinicalAnalytics',
    label: 'CLINICAL ANALYTICS',
    description: 'This column has a value getter and is not sortable.',
    type: 'string',
    width: 300,
  },
];

const rows2 = [
  {
    id: 1,
    user: 'Marie Lamouret',
    startTime: 'Feb 3, 2022 -4:36am',
    procedure: 'Infinity',
    duration: 35,
    clinicalAnalytics: 'Unavailable',
  },
  {
    id: 2,
    user: 'Marie Lamouret',
    startTime: 'Feb 3, 2022 -4:36am',
    procedure: 'Infinity',
    duration: 42,
    clinicalAnalytics: 'Unavailable',
  },
];

interface Column3 {
  id: 'email' | 'name' | 'date' | 'vrkit' | 'institution' | 'type' | 'role';
  label: string;
  minWidth?: number;
  align?: 'right';
  format?: (value: number) => string;
}
const columns3: Column3[] = [
  { id: 'email', label: 'EMAIL', minWidth: 170 },
  { id: 'name', label: 'NAME', minWidth: 100 },
  {
    id: 'date',
    label: 'DATE',
    minWidth: 170,
    format: (value: number) => value.toLocaleString('en-US'),
  },
  {
    id: 'vrkit',
    label: 'VRKIT USED',
    minWidth: 170,
    format: (value: number) => value.toLocaleString('en-US'),
  },
  {
    id: 'institution',
    label: 'INSTITUTION',
    minWidth: 170,
    format: (value: number) => value.toFixed(2),
  },
  {
    id: 'type',
    label: 'TYPE',
    minWidth: 170,
    format: (value: number) => value.toFixed(2),
  },
  {
    id: 'role',
    label: 'ROLE',
    minWidth: 170,
    format: (value: number) => value.toFixed(2),
  },
];
const rows3 = [
  {
    email: 'mz@yahoo.ca',
    name: 'Marie Louvre',
    date: 'Feb 3, 2022 -4:36am',
    vrkit: 'xyz123qweasd',
    institution: 'GCSOM',
    type: 'Unavailable',
    role: 'individual',
  },
  {
    email: 'maz@yahoo.ca',
    name: 'Marie ',
    date: 'Feb 4, 2022 -4:36am',
    vrkit: 'xyz123qweasd',
    institution: 'GCSOM',
    type: 'Unavailable',
    role: 'individual',
  },
  {
    email: 'maaz@yahoo.ca',
    name: 'Marie Lamouret',
    date: 'Feb 5, 2022 -4:36am',
    vrkit: 'xyz123qweasd',
    institution: 'GCSOM',
    type: 'Unavailable',
    role: 'individual',
  },
  {
    email: 'rodney@yahoo.ca',
    name: 'Rodney',
    date: 'Feb 6, 2022 -4:36am',
    vrkit: 'xyz123qweasd',
    institution: 'GCSOM',
    type: 'Unavailable',
    role: 'individual',
  },
  {
    email: 'j.k.rowlings@yahoo.ca',
    name: 'Joking Rowlings',
    date: 'Feb 7, 2022 -4:36am',
    vrkit: 'xyz123qweasd',
    institution: 'GCSOM',
    type: 'Unavailable',
    role: 'individual',
  },
  {
    email: 'mrKennedys@yahoo.ca',
    name: 'Mr Kennedy',
    date: 'Feb 8, 2022 -4:36am',
    vrkit: 'xyz123qweasd',
    institution: 'GCSOM',
    type: 'Unavailable',
    role: 'individual',
  },
];
export default meta;
const Template: Story = () => (
  <Tables type="allSessions" rowsData={rows} col={columns} />
);

export const AllSessions = Template.bind({});

export const MyDashboard = () => (
  <Tables type="MyDashboard" rowsData={rows2} col={columns2} />
);

export const LoginTrackerAndAllUsers = () => (
  <Tables type="loginTracker" rowsData={rows3} col={columns3} />
);
