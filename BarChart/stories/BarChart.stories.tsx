import React from 'react';
import BarChart from '../src/BarChart';

export default {
  title: 'Bar Chart',
  component: BarChart,
};

export const barchart = () => <BarChart title="Chart subtitle" combined={34} />;
