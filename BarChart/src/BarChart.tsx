import React from 'react';
import { Typography } from '@mui/material';
import { CategoryScale } from 'chart.js';
import Chart from 'chart.js/auto';
import { Bar } from 'react-chartjs-2';
Chart.register(CategoryScale);

const date = () => {
  let past30Days: any = [];
  let today = new Date();
  let todayDate =
    today.toLocaleString('default', { month: 'short' }) +
    ' ' +
    today.getDate() +
    ', ' +
    today.getFullYear();

  let yesterday = new Date(today);
  yesterday.setDate(yesterday.getDate() - 30);
  let currDate =
    yesterday.toLocaleString('default', { month: 'short' }) +
    ' ' +
    yesterday.getDate() +
    ', ' +
    yesterday.getFullYear();
  let dateRange = currDate + ' - ' + todayDate;
  past30Days.push(dateRange);

  console.log('Date array IS  ', past30Days);

  return past30Days.reverse();
};

const BarChart = (props: any) => {
  const labels = date();
  const data = {
    labels: labels,
    datasets: [
      {
        label: props.title,
        data: [props.combined],
        fill: false,
        borderColor: 'rgb(75, 192, 192)',
        tension: 0.1,
        backgroundColor: '#1f55de',
      },
    ],
  };
  date();

  return (
    <div style={{ marginRight: 5 }}>
      <Typography>{date}</Typography>
      <Bar
        data={data}
        width={300}
        height={200}
        options={{ maintainAspectRatio: true }}
      />
    </div>
  );
};

export default BarChart;
