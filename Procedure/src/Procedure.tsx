import React from 'react';
import {
  Typography,
  CardMedia,
  CardActions,
  Card,
  CardContent,
} from '@mui/material';
import Button from '~/Button/src/Button';

const Procedure = (props: any) => {
  return (
    <Card className="procedure">
      <CardMedia
        component="img"
        alt="green iguana"
        height="180"
        style={{ objectFit: 'contain' }}
        image="https://static.vecteezy.com/system/resources/previews/002/757/076/original/surgical-operation-flat-illustration-internal-medicine-anesthetist-and-surgeon-with-patient-cartoon-characters-medical-procedure-resuscitation-surgery-concept-isolated-on-white-background-vector.jpg"
      />
      <CardContent>
        <Typography
          gutterBottom
          variant="h6"
          component="div"
          style={{ color: '#1f55de', fontSize: 16 }}
        >
          {props.title}
          {/*NON OPERATIVE SPINE*/}
        </Typography>
        <Typography
          gutterBottom
          style={{ fontSize: '12px', color: 'grey' }}
          component="div"
        >
          {props.firm}
          {/* PrecisionOS*/}
        </Typography>
        <div style={{ paddingTop: '20px' }}>
          <Typography variant="body2" style={{ color: 'black' }}>
            {props.description}
            {/* Proximal Femoral Nail is the treatment for intertrochanteric
            fractures. The proximal femoral nail procedure guides you through
            appropriate fracture reduction and nail placements.*/}
          </Typography>
        </div>
      </CardContent>
      <CardActions
        sx={{ justifyContent: 'flex-end', paddingRight: 2, paddingBottom: 2 }}
      >
        <Button title="View More" color="blue" />
        {/* <Button variant="contained" size="small" style={{backgroundColor:"#1f55de"}}>View More</Button> */}
      </CardActions>
    </Card>
  );
};

export default Procedure;
