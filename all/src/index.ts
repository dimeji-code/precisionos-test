import merge from 'deepmerge';

import { typography } from '~/typography/src';
import { colors } from '~/colors/src';

export const theme = merge.all([typography, colors], {
  arrayMerge: (t, s) => [...s, ...t],
});
