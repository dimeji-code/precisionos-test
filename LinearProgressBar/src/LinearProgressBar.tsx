import React from 'react';
import LinearProgress from '@mui/material/LinearProgress';
import { Box, styled, Typography } from '@mui/material';
import '../src/linear.scss';
const StyledLinearProgress = styled(LinearProgress)({
  backgroundColor: 'rgba(0, 0, 0, 0.08)',
  padding: 4,
  borderRadius: 5,
});

const LinearProgressBar = (props: any) => {
  const classArray = [
    'linearProg1',
    'linearProg2',
    'linearProg3',
    'linearProg4',
    'linearProg5',
    'linearProg6',
    'linearProg7',
  ];
  const colorClass = classArray[Math.floor(Math.random() * classArray.length)];

  return (
    <div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          marginBottom: 10,
        }}
      >
        <Typography style={{ fontSize: 14 }}>{props.title}</Typography>
        <Typography style={{ fontSize: 14 }}>{props.procedureCount}</Typography>
      </div>
      <Box sx={{ width: '100%' }}>
        <StyledLinearProgress
          className={colorClass}
          variant="determinate"
          value={15}
        />
        {/* <LinearProgress className={colorClass}    /> */}
      </Box>
    </div>
  );
};

export default LinearProgressBar;
