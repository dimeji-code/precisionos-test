import React from 'react';
import LinearProgressBar from '../src/LinearProgressBar';

export default {
  title: 'Linear Progress Bar',
  component: LinearProgressBar,
};

export const progressBar = () => (
  <LinearProgressBar title="Name" procedureCount="Count" />
);
