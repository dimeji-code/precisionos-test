import React from 'react';
import Header from '../src/Header';

export default {
  title: 'Header',
  component: Header,
};

export const header = () => (
  <Header logo="https://www.precisionostech.com/wp-content/uploads/2019/10/precision-os-logo.png" />
);
