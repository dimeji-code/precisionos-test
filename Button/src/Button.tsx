import React from 'react';
import { Typography, Box } from '@mui/material';
import '../src/button.scss';
import { colors } from '~/colors/src';

const Button = (props: any) => {
  let minWidth = props.icon != null ? 220 : 50;
  let maxWidth: number;
  let textColor: string;
  let buttonColor: string;
  let centered: any;
  if (props.size == 'sm') {
    maxWidth = 100;
  } else {
    maxWidth = 200;
  }
  if (props.color == 'primary') {
    buttonColor = colors.colors.primary;
    textColor = colors.colors.secondary;
  } else if (props.color == 'secondary') {
    buttonColor = colors.colors.secondary;
    textColor = colors.colors.primary;
  } else {
    //DEFAULT SETTINGS
    buttonColor = colors.colors.primary;
    textColor = colors.colors.secondary;
  }

  let buttonStyles: any = {
    backgroundColor: buttonColor,
    maxHeight: 30,
    paddingLeft: 10,
    padding: 6,
    minWidth: minWidth,
    borderRadius: 2,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    maxWidth: maxWidth,
  };
  if (props.icon == null) {
    centered = { justifyContent: 'center' };
  } else {
    centered = null;
  }

  return (
    <div
      onClick={props.onPress}
      className="defaultButton"
      style={{ ...buttonStyles, ...centered }}
    >
      <Box>{props.icon}</Box>
      <Box>
        <Typography style={{ color: textColor, fontSize: 15 }}>
          {' '}
          {props.title}
        </Typography>
      </Box>
    </div>
  );
};

export default Button;
