import React from 'react';
import { Meta, Story } from '@storybook/react';
import Button from '../src/Button';
import GroupIcon from '@mui/icons-material/Group';
import { colors } from '~/colors/src';

const meta: Meta = {
  title: 'Custom Button',
  component: Button,
};

export default meta;
const Template: Story = () => (
  <Button
    title="Primary Button"
    icon={
      <GroupIcon
        style={{ paddingRight: '5px', color: colors.colors.secondary }}
      />
    }
    color="blue"
  />
);

export const Default = Template.bind({});

export const Secondary = () => (
  <Button
    title="Light Button"
    color="secondary"
    icon={
      <GroupIcon
        style={{ paddingRight: '5px', color: colors.colors.primary }}
      />
    }
  />
);
export const Tertiary = () => (
  <Button title="Small" size="sm" color="primary" />
);
