import React from 'react';
import LineChart from '../src/LineChart';

export default {
  title: 'Line Chart',
  component: LineChart,
};

export const linechart = () => (
  <LineChart
    title="daily # of procedures"
    yData={[65, 59, 80, 81, 56, 55, 40]}
  />
);
