import React from 'react';
import { Typography } from '@mui/material';
import { CategoryScale, registerables } from 'chart.js';
import Chart from 'chart.js/auto';
import { Line } from 'react-chartjs-2';
Chart.register(CategoryScale);

const date = () => {
  let past7Days: any = [];
  let today = new Date();

  for (let i = 0; i < 7; i++) {
    let yesterday = new Date(today);
    yesterday.setDate(yesterday.getDate() - i);
    let currDate =
      yesterday.toLocaleString('default', { month: 'short' }) +
      ' ' +
      yesterday.getDate() +
      ', ' +
      yesterday.getFullYear();
    past7Days.push(currDate);
  }
  console.log('Date array IS  ', past7Days);
  // console.log("TODAY IS  ", todayDate);

  return past7Days.reverse();
};

const LineChart = (props: any) => {
  const labels = date();
  const data = {
    labels: labels,
    datasets: [
      {
        label: props.title,
        data: props.yData,
        fill: false,
        borderColor: '#1f55de',
        tension: 0.1,
        backgroundColor: '#1f55de',
      },
    ],
  };
  date();

  return (
    <div>
      <Typography>{date}</Typography>
      <Line
        data={data}
        width={340}
        height={240}
        options={{ maintainAspectRatio: true }}
      />
    </div>
  );
};

export default LineChart;
