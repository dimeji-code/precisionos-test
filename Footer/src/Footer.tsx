import { Typography, Card } from '@mui/material';
import React from 'react';

const Footer = () => {
  const getYear = () => {
    let year = new Date().getFullYear();
    return year;
  };
  return (
    <Card
      style={{
        width: '100%',
        height: '60px',
        marginTop: '10px',
        position: 'sticky',
        bottom: 0,
      }}
    >
      {/* <Card style={{width: '100%', height: '60px',marginTop:"10px",position:"relative",bottom:0}}> */}
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%',
        }}
      >
        <Typography>
          ©{getYear()} PrecisionOS. All right reserved. Privacy Policy
        </Typography>
      </div>
    </Card>
  );
};

export default Footer;
